package com.example.splashscreen

import android.app.Application
import com.example.splashscreen.di.dataSourceModule
import com.example.splashscreen.di.repoModule
import com.example.splashscreen.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(dataSourceModule, repoModule, viewModelModule))
        }
    }
}