package com.example.splashscreen.data.mocks

import com.example.splashscreen.data.models.StoreInfo

class StoreInfoMock {

    fun loadStoreInfo(): StoreInfo {
        return StoreInfo(
            1,
            "Sejofradi Store",
            "https://toppng.com/uploads/preview/payroll-web-buttons-icons-05-convenience-store-icon-11553443938ol347nla5d.png",
            "En algún lugar del mundo",
            "En esta tienda vendemos hasta lo que no existe."
        )
    }
}