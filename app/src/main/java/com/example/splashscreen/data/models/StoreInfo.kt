package com.example.splashscreen.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StoreInfo(
    @PrimaryKey var id: Int? = null,
    val name: String? = null,
    val image: String? = null,
    val address: String? = null,
    val description: String? = null,
    val lat: Double? = null,
    val lng: Double? = null
)