package com.example.splashscreen.data.repositories

import com.example.splashscreen.data.mocks.ProductMock
import com.example.splashscreen.data.models.Product
import com.example.splashscreen.utils.Constants
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class ProductRepository(private val dataSource: ProductMock, private val dataSourceFirebase: FirebaseFirestore) {

    private val db: CollectionReference = dataSourceFirebase.collection(Constants.PRODUCT_COLLECTION)

    suspend fun loadProduct(): List<Product>{
/*        return withContext(Dispatchers.IO){
            dataSource.loadProducts()
        }*/
        val snapshot = db.get().await()
        return snapshot.toObjects(Product::class.java)
    }
}