package com.example.splashscreen.data.repositories


import com.example.splashscreen.data.mocks.StoreInfoMock
import com.example.splashscreen.data.models.StoreInfo
import com.example.splashscreen.utils.Constants
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class HomeRepository(private val dataSource: StoreInfoMock, private val dataSourceFirebase: FirebaseFirestore) {

    private val db: CollectionReference = dataSourceFirebase.collection(Constants.STORE_COLLECTION)

    suspend fun loadStoreInfo(): StoreInfo {
        val snapshot = db.get().await()
        return snapshot.toObjects(StoreInfo::class.java)[0]
    }

}