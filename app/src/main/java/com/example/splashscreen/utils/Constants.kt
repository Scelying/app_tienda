package com.example.splashscreen.utils

class Constants {
    companion object{
        val PRODUCT_COLLECTION = "products"
        val STORE_COLLECTION = "store"
    }
}