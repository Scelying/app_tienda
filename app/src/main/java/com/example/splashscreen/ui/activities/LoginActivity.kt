package com.example.splashscreen.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.splashscreen.R
import com.example.splashscreen.databinding.ActivityLoginBinding
import com.example.splashscreen.databinding.FragmentProfileBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}