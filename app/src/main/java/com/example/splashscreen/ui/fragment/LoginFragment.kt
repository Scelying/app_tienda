package com.example.splashscreen.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.splashscreen.R
import com.example.splashscreen.databinding.FragmentLoginBinding
import com.example.splashscreen.ui.activities.MainActivity
import com.example.splashscreen.ui.viewmodels.LoginViewModel
import com.example.splashscreen.utils.isValidEmail
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null

    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_login, container, false)
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.buttonRegistrar.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment3)
        }

        binding.buttonIngresar.setOnClickListener {
            var isValid = true

            if (!binding.loginEmail.text.isValidEmail()) {
                isValid = false
                binding.loginEmailLayout.error = "Correo electrónico no válido"
            } else {
                binding.loginEmailLayout.error = null
            }

            if (binding.loginPassword.text.toString().length < 3) {
                isValid = false
                binding.loginPasswordLayout.error = "Contraseña muy corta"
            } else {
                binding.loginPasswordLayout.error = null
            }

            if (isValid) {
                loginViewModel.login(
                    binding.loginEmail.text.toString(),
                    binding.loginPassword.text.toString()
                )
            }
        }

        loginViewModel.user.observe(viewLifecycleOwner, Observer { user ->
            if (user != null) {
                val intent = Intent(requireActivity(), MainActivity::class.java)
                startActivity(intent)
                requireActivity().finish()
            }
        })
        loginViewModel.error.observe(viewLifecycleOwner, Observer { error ->
            Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
        })
    }

    /*override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonRegistrar.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_RegisterFragment)
        }
    }*/

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}