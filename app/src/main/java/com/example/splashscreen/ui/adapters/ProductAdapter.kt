package com.example.splashscreen.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.splashscreen.ui.listener.OnProductListener
import com.example.splashscreen.data.models.Product
import com.example.splashscreen.databinding.ItemProductBinding


class ProductAdapter(var items: List<Product>): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var listener : OnProductListener? = null

    class ViewHolder(val view: ItemProductBinding): RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemProductBinding.inflate(inflater,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        val view = holder.view

        view.productItemName.text = item.name
        view.productItemPrice.text = item.price
        Glide.with(holder.itemView).load(item.image).into(view.productItemPhoto);

        view.root.setOnClickListener{
            listener?.onClick(item)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun newDataset(products: List<Product>){
        items = products
        notifyDataSetChanged()
    }
}