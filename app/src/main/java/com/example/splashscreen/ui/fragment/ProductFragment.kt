package com.example.splashscreen.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.splashscreen.R
import com.example.splashscreen.ui.listener.OnProductListener
import com.example.splashscreen.data.models.Product
import com.example.splashscreen.databinding.FragmentProductBinding
import com.example.splashscreen.ui.adapters.ProductAdapter
import com.example.splashscreen.ui.viewmodels.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ProductFragment : Fragment() {

    private var _binding: FragmentProductBinding? = null
    private val binding get() = _binding!!

    private lateinit var productAdapter: ProductAdapter
    private lateinit var productManager: GridLayoutManager

    private val productViewModel: ProductViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        productAdapter = ProductAdapter(
            listOf()
        )

        productAdapter.listener = object : OnProductListener {
            override fun onClick(product: Product) {
                Log.d("Click", product.name!!)
                Log.d("Click", product.image!!)
                productViewModel.selectProduct(product)
                findNavController().navigate(R.id.action_productFragment_to_productDetailFragment)
            }
        }

        productManager = GridLayoutManager(requireContext(), 2)

        binding.recyclerProduct.apply {
            adapter = productAdapter
            layoutManager = productManager
        }

        productViewModel.loadProducts()
        productViewModel.products.observe(viewLifecycleOwner, Observer { products -> productAdapter.newDataset(products) })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}