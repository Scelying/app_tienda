package com.example.splashscreen.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.splashscreen.data.repositories.UserRepository
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.launch

class SplashViewModel(private val userRepo: UserRepository) : ViewModel() {

    private var _user: MutableLiveData<FirebaseUser?> = MutableLiveData()
    val user: LiveData<FirebaseUser?> get() = _user

    fun loggedIn() {
        viewModelScope.launch {
            _user.postValue(userRepo.loggedIn())
        }
    }
}