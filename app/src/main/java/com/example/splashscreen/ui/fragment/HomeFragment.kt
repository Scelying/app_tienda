package com.example.splashscreen.ui.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.splashscreen.R
import com.example.splashscreen.data.models.StoreInfo
import com.example.splashscreen.databinding.FragmentHomeBinding
import com.example.splashscreen.ui.viewmodels.HomeViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ktx.awaitMap
import com.google.maps.android.ktx.awaitMapLoad
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.osmdroid.config.Configuration.*
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker




class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    private val homeViewModel: HomeViewModel by sharedViewModel()

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        getInstance().load(requireContext(), PreferenceManager.getDefaultSharedPreferences(requireContext()))
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        homeViewModel.loadStoreInfo()
        homeViewModel.info.observe(viewLifecycleOwner, Observer { store ->
            binding.titleHome.text = store.name
            binding.homeAddress.text = store.address
            binding.homeDescription.text = store.description
            Glide.with(binding.root).load(store.image).into(binding.imageHome)

//            if(ContextCompat.checkSelfPermission(requireContext(),
//                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(requireContext(),
//                    Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED) {
//
//                binding.mapOsmHome.visibility = View.VISIBLE
//                binding.mapOsmHome.setTileSource(TileSourceFactory.MAPNIK)
//
//                val mapController = binding.mapOsmHome.controller
//                mapController.setZoom(20.0)
//
//                val startPoint = GeoPoint(store.lat!!, store.lng!!)
//                mapController.setCenter(startPoint)
//
//                val marker = Marker(binding.mapOsmHome)
//                marker.position = startPoint
//
//                marker.setAnchor(
//                    Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM
//                )
//
//                marker.title = store.name
//                binding.mapOsmHome.overlays.add(marker)
//            }

            val mapFragment = childFragmentManager.findFragmentById(R.id.map_home) as SupportMapFragment

            lifecycleScope.launchWhenStarted {

                val googleMap = mapFragment.awaitMap()
                addMarker(googleMap, store)
                googleMap.awaitMapLoad()

                val bounds = LatLngBounds.builder().include(LatLng(store.lat!!, store.lng!!)).build()
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20))
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(store.lat!!, store.lng!!), 15.0f))

            }

        })


    }

    private fun addMarker(map: GoogleMap, store: StoreInfo) {
        map.addMarker(
            MarkerOptions()
                .position(LatLng(store.lat!!, store.lng!!))
                .title(store.name)
        )
    }

}