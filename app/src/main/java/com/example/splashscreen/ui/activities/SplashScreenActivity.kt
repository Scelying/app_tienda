package com.example.splashscreen.ui.activities

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.airbnb.lottie.LottieAnimationView
import com.example.splashscreen.R
import com.example.splashscreen.databinding.ActivitySplashScreenBinding
import com.example.splashscreen.ui.viewmodels.SplashViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var splash: LottieAnimationView
    private lateinit var binding: ActivitySplashScreenBinding

    private val splashViewModel: SplashViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*setContentView(R.layout.activity_splash_screen)*/
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        splashViewModel.loggedIn()

        splash = findViewById(R.id.splashanimation)
        splash.playAnimation()

        splash.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                splashViewModel.user.observe(this@SplashScreenActivity, { user ->
                    if (user == null){
                        val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                        startActivity(intent)
                    } else{
                        val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                    finish()
                })
/*                val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                startActivity(intent)*/
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationRepeat(p0: Animator?) {

            }

        })
    }
}