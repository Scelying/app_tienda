package com.example.splashscreen.ui.listener

import com.example.splashscreen.data.models.Product

interface OnProductListener {

    fun onClick(product : Product)
}