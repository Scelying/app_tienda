package com.example.splashscreen.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.splashscreen.databinding.FragmentRegisterBinding
import com.example.splashscreen.ui.viewmodels.LoginViewModel
import com.example.splashscreen.utils.isValidEmail
import org.koin.androidx.viewmodel.ext.android.viewModel


class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_register, container, false)
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.buttonRegistrar.setOnClickListener {
            var isValid = true

            if (!binding.signupEmail.text.isValidEmail()) {
                isValid = false
                binding.signupEmailLayout.error = "Correo electrónico no válido"
            } else {
                binding.signupEmailLayout.error = null
            }

            if (binding.signupName.text.toString().length < 3) {
                isValid = false
                binding.signupNameLayout.error = "El nombre es muy corto"
            } else {
                binding.signupNameLayout.error = null
            }

            if (binding.signupPassword.text.toString().length < 3) {
                isValid = false
                binding.signupPasswordLayout.error = "Contraseña muy corta"
            } else {
                binding.signupPasswordLayout.error = null
            }

            if (binding.signupPasseordval.text.toString() != binding.signupPassword.text.toString()) {
                isValid = false
                binding.signupPasswordvalLayout.error = "La contraseña no coincide"
            } else {
                binding.signupPasswordvalLayout.error = null
            }

            if (isValid) {
                loginViewModel.signUp(
                    binding.signupEmail.text.toString(),
                    binding.signupName.text.toString(),
                    binding.signupPassword.text.toString()
                )
            }

            loginViewModel.user.observe(viewLifecycleOwner, Observer { user ->
                Toast.makeText(requireContext(), "Usuario registrado", Toast.LENGTH_SHORT).show()
                requireActivity().onBackPressed()
            })

            loginViewModel.error.observe(viewLifecycleOwner, Observer { error ->
                Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
            })

        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}