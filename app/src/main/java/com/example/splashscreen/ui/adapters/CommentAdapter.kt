package com.example.splashscreen.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.splashscreen.R
import com.example.splashscreen.data.models.Comentarios
import com.example.splashscreen.databinding.ItemComentariosBinding

class CommentAdapter (val items: List<Comentarios>): RecyclerView.Adapter<CommentAdapter.ViewHolder>(){

    class ViewHolder(val view:ItemComentariosBinding ): RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemComentariosBinding.inflate(inflater,parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        val view = holder.view
        view.nombreUsuarioN.text = item.name
        view.comentarioUsuarioN.text = item.message
        view.fotoUsuario.setImageResource(R.mipmap.ic_launcher)


    }

    override fun getItemCount(): Int {
        return items.size
    }
}

