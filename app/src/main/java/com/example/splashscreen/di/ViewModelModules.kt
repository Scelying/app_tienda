package com.example.splashscreen.di

import com.example.splashscreen.ui.viewmodels.HomeViewModel
import com.example.splashscreen.ui.viewmodels.LoginViewModel
import com.example.splashscreen.ui.viewmodels.ProductViewModel
import com.example.splashscreen.ui.viewmodels.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { ProductViewModel(get()) }
    viewModel { HomeViewModel(get()) }
}