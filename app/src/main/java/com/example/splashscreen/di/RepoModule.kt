package com.example.splashscreen.di

import com.example.splashscreen.data.repositories.HomeRepository
import com.example.splashscreen.data.repositories.ProductRepository
import com.example.splashscreen.data.repositories.UserRepository
import org.koin.dsl.module

val repoModule = module {
    single { UserRepository(get(), get()) }

    single {
        ProductRepository(get(), get())
    }

    single {
        HomeRepository(get(), get())
    }
}