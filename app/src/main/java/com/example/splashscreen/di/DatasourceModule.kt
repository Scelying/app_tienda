package com.example.splashscreen.di

import com.example.splashscreen.data.mocks.ProductMock
import com.example.splashscreen.data.mocks.StoreInfoMock
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.koin.dsl.module

val dataSourceModule = module {

    single { FirebaseFirestore.getInstance() }
    single { FirebaseAuth.getInstance() }
    single { FirebaseStorage.getInstance().getReference() }

    single{
        ProductMock()
    }

    single {
        StoreInfoMock()
    }
}